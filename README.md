# fediblogs
RSS Aggregator bot to manage several blogs and post their articles to a Mastodon server.  
Mastodon server local users can add themselves their RSS 2.0 compliant blogs to the bot by issuing the following post from their Mastodon account:  

@your_configured_bot_username blog https://exemple.blog/rss  
  
The bot will add the new added blog to its database and start posting its entries.


### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account  
-   Mastodon server system account (the one running the server)

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.  

2. Run `python db-setup.py` to setup and create new Postgresql database and needed tables.  

3. Run `python setup.py` to setup the Mastodon bot account & access tokens, Mastodon server's database name & user and also the username of the Mastodon local account who will become the bot admin by posting `@your_bot_username admin`.

5. Use your favourite scheduling method to set `python admin.py` and `python fediblogs.py` to run every minute.  

