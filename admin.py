import sys
import os
import os.path
import re
import requests
from datetime import datetime, timedelta
from mastodon import Mastodon
import psycopg2
import feedparser
import pdb

def cleanhtml(raw_html):

    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):

    s = s.replace("&apos;", "'")
    return s

def replying():

    reply = False

    content = cleanhtml(text)

    content = unescape(content)

    try:

        start = content.index("@")
        end = content.index(" ")
        if len(content) > end:

            content = content[0: start:] + content[end +1::]

        neteja = content.count('@')

        i = 0
        while i < neteja :

            start = content.rfind("@")
            end = len(content)
            content = content[0: start:] + content[end +1::]
            i += 1

        question = content.lower()

        query_word = question
        query_word_length = len(query_word)

        if query_word[:5]  == 'admin':

            reply = True

        if query_word[:4]  == 'blog':

            reply = True

        return (reply, query_word)

    except ValueError as v_error:

        print(v_error)

def add_admin(username, mastodon_hostname):

    insert_error = ''

    insert_sql = 'INSERT INTO users(username, user_domain, created_at, admin) VALUES(%s, %s, %s, %s)'

    now = datetime.now()

    try:

        conn = None

        conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(insert_sql, (username, mastodon_hostname, now, 't'))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        insert_error = error.pgcode

        sys.stdout.write(f'\n{str(error)}\n')

    finally:

        if conn is not None:

            conn.close()

    return insert_error

def add_blog(blog_url, username):

    insert_error = ''

    is_added = False

    insert_sql = 'INSERT INTO blogs(blog_url, created_at, created_by) VALUES(%s, %s, %s)'

    now = datetime.now()

    try:

        conn = None

        conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(insert_sql, (blog_url, now, username))

        conn.commit()

        cur.close()

        is_added = True

    except (Exception, psycopg2.DatabaseError) as error:

        insert_error = error.pgcode

        sys.stdout.write(f'\n{str(error)}\n')

    finally:

        if conn is not None:

            conn.close()

    return (is_added, insert_error)

def check_rss(blog_url):

    is_rss = False

    try:

        user_agent = {'User-agent': 'Mozilla/5.0'}

        response = requests.get(blog_url, headers = user_agent, timeout=3)

        try:

            newsfeeds = feedparser.parse(blog_url)

            if newsfeeds.version == 'rss20' or newsfeeds.version == 'rss10':

                is_rss = True

                return is_rss

        except:

            print(newsfeeds.status)

    except requests.exceptions.ConnectionError as conn_error:

        print(f'{blog_url} {conn_error}')

        return is_rss

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
    bot_username = get_parameter("bot_username", config_filepath)
    bot_admin = get_parameter("bot_admin", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname, bot_username, bot_admin)

def db_config():

    # Load db configuration from config file
    config_filepath = "config/db_config.txt"
    fediblogs_db = get_parameter("fediblogs_db", config_filepath)
    fediblogs_db_user = get_parameter("fediblogs_db_user", config_filepath)

    return (fediblogs_db, fediblogs_db_user)

def get_parameter( parameter, file_path ):

    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname, bot_username, bot_admin = mastodon()

    fediblogs_db, fediblogs_db_user = db_config()

    now = datetime.now()

    bot_id = mastodon.me().id

    bot_notifications = mastodon.notifications()

    if len(bot_notifications) == 0:

        print('No mentions')
        sys.exit(0)

    i = 0

    while i < len(bot_notifications):

        notification_id = bot_notifications[i].id

        if bot_notifications[i].type != 'mention':

            i += 1

            mastodon.notifications_dismiss(notification_id)

            continue

        account_id = bot_notifications[i]

        username = bot_notifications[i].account.username

        status_id = bot_notifications[i].status.id

        text  = bot_notifications[i].status.content

        visibility = bot_notifications[i].status.visibility

        reply, query_word = replying()

        if reply == True:

            if query_word[:5] == 'admin' and not '@' in bot_notifications[i].account.acct:

                is_admin = True if username+'@'+mastodon_hostname == bot_admin else False

                if is_admin:

                    insert_error = add_admin(username, mastodon_hostname)

                    if insert_error == '23505':

                        toot_text = f'@{username}\n\nja ets admin a la base de dades!'

                    elif insert_error == '':

                        toot_text = f"@{username}\n\nara ets l'administrador de {bot_username}!"

                    else:

                        toot_text = f'@{username}\n\nerror {insert_error}'

                else:

                    toot_text = f'@{username}\n\nja tinc un administrador.'

            if query_word[:4] == 'blog' and not '@' in bot_notifications[i].account.acct:

                blog_url = query_word[5:]

                is_rss = check_rss(blog_url)

                if is_rss:

                    is_added, insert_error = add_blog(blog_url, username)

                    if is_added:

                        toot_text = f'@{username}\n\nblog {blog_url} afegit amb èxit!'

                    else:

                        if insert_error == '23505':

                            toot_text = f'@{username}\n\n{blog_url} ja és a la base de dades.'

                        else:

                            toot_text = f'@{username}\n\n{blog_url} no es pot afegir, error {insert_error}'

                else:

                    toot_text = f'@{username}\n\n{blog_url} no és un feed RSS o no funciona o no existeix...'

        mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

        mastodon.notifications_dismiss(notification_id)

        i += 1
