import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import pdb

def get_dbconfig():

    # Load configuration from config file
    config_filepath = "config/db_config.txt"
    fediblogs_db = get_parameter("fediblogs_db", config_filepath)
    fediblogs_db_user = get_parameter("fediblogs_db_user", config_filepath)

    return (config_filepath, fediblogs_db, fediblogs_db_user)

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def create_db():

    create_error = ''

    conn = None

    try:

      conn = psycopg2.connect(dbname='postgres',
          user=fediblogs_db_user, host='',
          password='')

      conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

      cur = conn.cursor()

      print("Creating database " + fediblogs_db + ". Please wait...")

      cur.execute(sql.SQL("CREATE DATABASE {}").format(
              sql.Identifier(fediblogs_db))
          )
      print("Database " + fediblogs_db + " created!")

    except (Exception, psycopg2.DatabaseError) as error:

        create_error = error.pgcode

        sys.stdout.write(f'\n{str(error)}\n')

    finally:

        if conn is not None:

            conn.close()

    return create_error

def check_db_conn():

    try:

        conn = None

        conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

        os.remove(config_filepath)

        sys.exit('Exiting. Run db-setup again with right parameters')

    if conn is not None:

        print(f'\nfediblogs parameters saved to db-config.txt!\n')

def write_parameter( parameter, file_path ):

    if not os.path.exists('config'):

        os.makedirs('config')
    
    print(f'Setting up fediblogs parameters...\n')

    fediblogs_db = input("fediblogs db name: ")
    fediblogs_db_user = input("fediblogs db user: ")

    with open(file_path, "w") as text_file:

        print("fediblogs_db: {}".format(fediblogs_db), file=text_file)
        print("fediblogs_db_user: {}".format(fediblogs_db_user), file=text_file)

def create_table(db, db_user, table, sql):

    conn = None

    try:

        conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        print(f'Creating table.. {table}')

        cur.execute(sql)

        conn.commit()

        print(f'Table {table} created!\n')

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

#############################################################################################
# main

if __name__ == '__main__':

    config_filepath, fediblogs_db, fediblogs_db_user = get_dbconfig()

    create_error = create_db()

    if create_error == '':

        check_db_conn()

    else:

        if create_error == '42P04':

            sys.exit()

        else:

            os.remove(config_filepath)

            sys.exit()

    ############################################################
    # Create needed tables
    ############################################################

    db = fediblogs_db
    db_user = fediblogs_db_user

    table = "users"
    sql = f"create table {table} (user_id serial, username VARCHAR(10) PRIMARY KEY, user_domain VARCHAR(30), created_at timestamptz, admin boolean)"

    create_table(db, db_user, table, sql)

    table = "blogs"
    sql = f'create table {table} (blog_id serial, blog_url VARCHAR(50) PRIMARY KEY, created_at timestamptz, created_by VARCHAR(30))'

    create_table(db, db_user, table, sql)

    table = "posts"
    sql = f'create table {table} (link VARCHAR(300) PRIMARY KEY, toot_id BIGINT, blog_id INT, created_at timestamptz)'

    create_table(db, db_user, table, sql)

    sys.exit(f'Done!\nNow you can run setup.py\n')
