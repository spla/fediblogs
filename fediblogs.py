import os
import requests
from io import BytesIO
import feedparser
from mastodon import Mastodon
import psycopg2
import sys
from datetime import datetime
import time
from langdetect import detect
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
import pdb

def get_blogs():

    blogs_id = []

    blogs_url = []

    blogs_creator = []

    try:

        conn = None

        conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute('select blog_id, blog_url, created_by from blogs')

        rows = cur.fetchall()

        for row in rows:

            blogs_id.append(row[0])

            blogs_url.append(row[1])

            blogs_creator.append(row[2])

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        insert_error = error.pgcode

        sys.stdout.write(f'\n{str(error)}\n')

    finally:

        if conn is not None:

            conn.close()

    return (blogs_id, blogs_url, blogs_creator)

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id=uc_client_id,
        client_secret=uc_client_secret,
        access_token=uc_access_token,
        api_base_url='https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers = {'Authorization': 'Bearer %s'%uc_access_token}

    return (mastodon, mastodon_hostname)

def db_config():

    # Load configuration from config file
    db_config_filepath = "config/db_config.txt"
    fediblogs_db =  get_parameter("fediblogs_db", db_config_filepath)
    fediblogs_db_user =  get_parameter("fediblogs_db_user", db_config_filepath)

    return (fediblogs_db, fediblogs_db_user)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = mastodon()

    fediblogs_db, fediblogs_db_user =  db_config()

    blogs_id, blogs_url, blogs_creator = get_blogs()

    #######################################################################

    blog = 0

    while blog < len(blogs_id):

        publish = 0

        try:

            resp = requests.get(blogs_url[blog], timeout=5)

        except requests.exceptions.ConnectTimeout as conntimeout:

            #logger.warning(f'Timeout when reading RSS {blogs_url[blog]}')

            pass

        content = BytesIO(resp.content)

        feed = feedparser.parse(content)

        for entry in feed.entries:

            title = entry['title']

            id = entry['id']

            link = entry['link']

            author = entry['author']

            ###################################################################
            # check database if feed is already published

            try:

                conn = None

                conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                cur = conn.cursor()

                cur.execute('select link from posts where link=(%s)', (link,))

                row = cur.fetchone()

                if row == None:

                    publish = 1

                else:

                    publish = 0

                cur.close()

            except (Exception, psycopg2.DatabaseError) as error:

                print(error)

            finally:

                if conn is not None:

                    conn.close()

            ###########################################################

            if publish == 1:

                tags_str = ''

                i = 0
                while i < len(entry.tags):

                    tags_str += f'#{entry.tags[i].term} '

                    i += 1

                toot_text = f'{title}\n\nper {author}\n\n{link}\n\n{tags_str}'

                print(f'Tooting...\n"{toot_text}')

                toot_dict = mastodon.status_post(toot_text, in_reply_to_id=None,)

                toot_id = toot_dict.id

                time.sleep(5)

                #########################################################

                insert_line = 'INSERT INTO posts(link, toot_id, blog_id, created_at) VALUES (%s, %s, %s, %s)'

                now = datetime.now()

                conn = None

                try:

                    conn = psycopg2.connect(database = fediblogs_db, user = fediblogs_db_user, password = "", host = "/var/run/postgresql", port = "5432")

                    cur = conn.cursor()

                    cur.execute(insert_line, (link, toot_id, blogs_id[blog], now))

                    conn.commit()

                    cur.close()

                except (Exception, psycopg2.DatabaseError) as error:

                    print(error)

                finally:

                    if conn is not None:

                        conn.close()

        blog += 1
