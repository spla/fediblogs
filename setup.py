import getpass
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError, MastodonIllegalArgumentError
import fileinput,re
import os
import sys

def create_dir():
  if not os.path.exists('secrets'):
    os.makedirs('secrets')

def create_file():
  if not os.path.exists('secrets/secrets.txt'):
    with open('secrets/secrets.txt', 'w'): pass
    print(secrets_filepath + " created!")

def create_config():
  if not os.path.exists('config'):
    os.makedirs('config')
  if not os.path.exists(config_filepath):
    print(config_filepath + " created!")
    with open('config/config.txt', 'w'): pass

def write_params():
  with open(secrets_filepath, 'a') as the_file:
    print("Writing secrets parameter names to " + secrets_filepath)
    the_file.write('uc_client_id: \n'+'uc_client_secret: \n'+'uc_access_token: \n')

def write_config():

    with open(config_filepath, 'a') as the_file:

        the_file.write('mastodon_hostname: \n')
        the_file.write('bot_username: \n')
        the_file.write('bot_admin: ')
        print("adding parameters 'mastodon_hostname', 'bot_username' & 'bot_admin' to "+ config_filepath)

def read_client_lines(self):

    client_path = 'app_clientcred.txt'

    with open(client_path) as fp:

        line = fp.readline()
        cnt = 1

        while line:

            if cnt == 1:

                print("Writing client id to " + secrets_filepath)
                modify_file(secrets_filepath, "uc_client_id: ", value=line.rstrip())

            elif cnt == 2:

                print("Writing client secret to " + secrets_filepath)
                modify_file(secrets_filepath, "uc_client_secret: ", value=line.rstrip())

            line = fp.readline()
            cnt += 1

def read_token_line(self):

    token_path = 'app_usercred.txt'

    with open(token_path) as fp:

        line = fp.readline()
        print("Writing access token to " + secrets_filepath)
        modify_file(secrets_filepath, "uc_access_token: ", value=line.rstrip())

def read_config_line():

    with open(config_filepath) as fp:

        line = fp.readline()
        modify_file(config_filepath, "mastodon_hostname: ", value=hostname)
        modify_file(config_filepath, "bot_username: ", value=bot_username)
        modify_file(config_filepath, "bot_admin: ", value=bot_admin)

def remove_secrets():

    if os.path.exists("secrets/secrets.txt"):

        print("Removing secrets/secrets.txt file..")

        os.remove("secrets/secrets.txt")

def log_in():

    error = 0

    try:

        global hostname
        global bot_username
        global bot_admin

        hostname = input("Enter Mastodon hostname: ")
        user_name = input("User name, ex. user@" + hostname +"? ")
        user_password = getpass.getpass("User password? ")
        bot_username = input("Bot's username, ex. fediblogs: ")
        app_name = input("This app name? ")
        bot_admin = input("bot admin username: ")
        bot_admin = f'{bot_admin}@{hostname}'

        Mastodon.create_app(app_name, scopes=["read","write"],

                to_file="app_clientcred.txt", api_base_url=hostname)

        mastodon = Mastodon(client_id = "app_clientcred.txt", api_base_url = hostname)

        mastodon.log_in(
            user_name,
            user_password,
            scopes = ["read", "write"],
            to_file = "app_usercred.txt"
        )

    except MastodonIllegalArgumentError as i_error:

        error = 1

        sys.stdout.write(f'\n{str(i_error)}\n')

    except MastodonNetworkError as n_error:

        error = 1

        sys.stdout.write(f'\n{str(n_error)}\n')

    except MastodonReadTimeout as r_error:

        error = 1

        sys.stdout.write(f'\n{str(r_error)}\n')

    except MastodonAPIError as a_error:

        error = 1

        sys.stdout.write(f'\n{str(a_error)}\n')

    finally:

        if error == 0:

            create_dir()
            create_file()
            write_params()

            client_path = 'app_clientcred.txt'
            read_client_lines(client_path)

            token_path = 'app_usercred.txt'
            read_token_line(token_path)

            if os.path.exists("app_clientcred.txt"):

                print("Removing app_clientcred.txt temp file..")
                os.remove("app_clientcred.txt")

            if os.path.exists("app_usercred.txt"):

                print("Removing app_usercred.txt temp file..")
                os.remove("app_usercred.txt")

            print("Secrets setup done!\n")

        else:

            remove_secrets()

            if os.path.exists("app_clientcred.txt"):

                print("Removing app_clientcred.txt file..")
                os.remove("app_clientcred.txt")

            sys.exit()

def modify_file(file_name,pattern,value=""):

    fh=fileinput.input(file_name,inplace=True)

    for line in fh:

        replacement=pattern + value
        line=re.sub(pattern,replacement,line)
        sys.stdout.write(line)

    fh.close()

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):

        print("File %s not found, creating it."%file_path)
        log_in()

    # Find parameter in file
    with open( file_path ) as f:

        for line in f:

            if line.startswith( parameter ):

                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + " Missing parameter %s "%parameter)
    sys.exit(0)

def get_host_bot( parameter, config_filepath ):

    # Check if secrets file exists
    if not os.path.isfile(config_filepath):

        print("File %s not found, creating it."%config_filepath)
        create_config()

    # Find parameter in file
    with open( config_filepath ) as f:

        for line in f:

            if line.startswith( parameter ):

                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(config_filepath + "  Missing parameter %s "%parameter)

    write_config()
    read_config_line()

    print("hostname & bot_username & bot_admin setup done!")
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_host_bot("mastodon_hostname", config_filepath)
    bot_username = get_host_bot("bot_username", config_filepath)
    bot_admin = get_host_bot("bot_admin", config_filepath)
